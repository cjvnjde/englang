'use strict';
const path = require('path');

module.exports = {
    entry: '/home',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    }
};