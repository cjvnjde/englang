module.exports = {
    "env": {
        "browser": true,
        "es6": true,
        "node": true,
        "mongo": true
    },
    "globals": {
      "App": true,
      "tinymce": true,
       "tinyMCE": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "sourceType": "module"
    },
    "rules": {
        "indent": [
            "error",
            4
        ],
        "linebreak-style": [
            "error",
            "windows"
        ],
        "quotes": [
            "warn",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ],
        "no-console":1,
        "no-unused-vars": 1
    }
};