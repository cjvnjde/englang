'use strict';
const User = require('../../models/user').User;

exports.get = function(req, res, next) {
    res.render('adminpanel/users', {
        title: 'Админка'
    });
};

exports.post = function(req, res, next) {
    User.find({}, {'username': true, 'accessRights': true}, (err, data) => {
        res.send(JSON.stringify(data));
    });
};
