'use strict';
const config = require('config');

exports.post = function(req, res, next){
    if (req.session) {
        req.session.destroy(err => console.log(err));
        res.clearCookie(config.get('session.key'), {path: config.get('session.cookie.path')});
        res.send('');
    }else{
        res.redirect('/');
    }

};