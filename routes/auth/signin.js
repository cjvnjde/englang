"use strict";
const AuthError = require('../../models/user').AuthError;
const HttpError = require('../../error/index').HttpError;
const User = require('../../models/user').User;

exports.get = function(req, res, next) {
    res.render('auth/signin', { title: 'Вход' });
};

exports.post = function(req, res, next) {

    //console.log(req.body.email);
    //console.log('');
    if(req.body.login != '' && req.body.password != '') {
        User.authorize(req.body.login, req.body.password, (err, user) => {
            if (err) {
                if (err instanceof AuthError) {
                    return next(new HttpError(403, err.message));
                } else {
                    return next(err);
                }
            }
            req.session.user = user._id;
            console.log(req.session.user+'_____________________');
            req.user = res.locals.user = user;
            res.redirect('/');
        });

    }else{
        res.redirect('/');
    }
};
