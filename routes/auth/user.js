"use strict";
exports.get = function(req, res, next) {
    res.render('user', { title: req.user.username });
};