'use strict';
const AuthError = require('../../models/user').AuthError;
const HttpError = require('../../error/index').HttpError;
const User = require('../../models/user').User;

exports.get = function(req, res, next) {
    res.render('auth/signup', { title: 'Регистрация' });
};

exports.post = function(req, res, next) {
    if(req.body.login !== '' && req.body.password !== '') {
        User.signup(req.body.login, req.body.password, req.body.email, (err, user) => {
            if (err) {
                if (err instanceof AuthError) {
                    return next(new HttpError(403, err.message));
                } else {
                    return next(err);
                }
            }
            req.session.user = user._id;
            req.user = res.locals.user = user;
            res.redirect('/');
        });
    }else{
        res.redirect('/');
    }
};
