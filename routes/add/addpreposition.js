'use strict';
const Preposition = require('../../models/preposition').Preposition;


exports.get = function(req, res, next) {
    res.render('add/addpreposition', {
        title: 'Добавьте предлог'
    });
};

exports.post = function(req, res, next) {
    Preposition.add(req.body.phrase, req.body.preposition, req.body.prepositionIndex);
    console.log(req.body.phrase + req.body.preposition + req.body.prepositionIndex);
    console.log('POST add prep');
    res.send('');
};
