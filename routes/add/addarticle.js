'use strict';
const Article = require('../../models/article').Article;

exports.get = function(req, res, next) {
    res.render('add/addarticle', {
        title: 'Добавьте статью.'
    });
};

exports.post = function(req, res, next) {
    Article.add(req.body.caption, req.body.data, req.user.username);
    res.send('');
};