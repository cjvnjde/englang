'use strict';
const Text = require('../../models/texts').Text;

exports.get = function(req, res, next) {
    res.render('add/addtext', {
        title: 'Добавьте текст.'
    });
};

exports.post = function(req, res, next) {
    Text.add(req.body.caption, req.body.data, req.user.username);
    res.send('');
};