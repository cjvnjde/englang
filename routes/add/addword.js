"use strict";
const Word = require('../../models/word').Word;


exports.get = function(req, res, next) {
    res.render('add/addword', {
        title: 'Добавьте слово'
    });
};

exports.post = function(req, res, next) {
    Word.add(req.body.eng, req.body.rus);
};
