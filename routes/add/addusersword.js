'use strict';
const User = require('../../models/user').User;

exports.get = function(req, res, next) {
    res.render('./add/addusersword', {
        title: 'Добавьте слово'
    });
};

exports.post = function(req, res, next) {
    console.log('POST');
    if(req.body.rus == null || req.body.rus == ''){
        res.send(JSON.stringify(req.user.words));
    }else {
        const newword = {'rus': req.body.rus, 'eng': req.body.eng};

        User.findOneAndUpdate({_id: req.user._id},
            {
                $pull: {
                    words: {eng: req.body.eng, rus: req.body.rus}
                }
            },
            (err, data)=>{
                if(err) console.log(err);
                if(!err){
                    User.findOneAndUpdate({_id: req.user._id}, {$push: {words: newword}}, {new: true}, (err, doc) => {
                        res.send(JSON.stringify(doc.words));
                    });
                }
            }
        );
    }
};
