'use strict';
const Article = require('../../models/article').Article;

exports.get = function(req, res, next) {
    console.log('deleting');
    console.log(req.params.id);
    Article.findOneAndRemove({_id: req.params.id}, err => {
        console.log(err);
    });
    res.redirect('/adminpanel/articles');
};