'use strict';
const Article = require('../../models/article').Article;

exports.get = function(req, res, next) {
    res.render('adminpanel/editarticle', {
        title: 'Изменение статьи',
        id: req.params.id
    });
};

exports.post = function(req, res, next) {
    if(req.params.id) {
        Article.findOne({_id: req.params.id}, (err, data) => {
            if (err || data === '' || data === null) {
                res.render('articles', {
                    title: 'Список статей'
                });
            } else {
                console.log(data.caption);
                res.send(JSON.stringify({
                    title: data.caption,
                    text1: data.text
                }));
            }
        });
    }else{
        Article.findOneAndUpdate({_id: req.body.id.trim()},
            {'text' : req.body.data, 'caption' : req.body.caption}).then((updatedDoc) => {});
    }
};