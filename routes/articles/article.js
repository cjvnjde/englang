'use strict';
const Article = require('../../models/article').Article;

exports.get = function(req, res, next) {
    Article.findOne({_id: req.params.id}, (err, data) => {
        if(err || data === '' || data === null) {
            res.render('pages/articles', {
                title: 'Список статей'
            });
        }else {
            res.render('pages/article', {
                title: data.caption,
                text1: data.text
            });
        }
    });
};