'use strict';
const Article = require('../../models/article').Article;

exports.get = function(req, res, next) {
    //console.log(req.params.id);
    res.render('pages/articles', {
        title: 'Список статей'
    });
};


exports.post = function(req, res, next) {
    Article.find({}, {'user_id': true, 'caption': true}, (err, data) => {
        console.log(data);
        res.send(JSON.stringify(data));
    });
};
