'use strict';
const translate = require('google-translate-api');

exports.post = function(req, res, next) {
    if(req.body.eng.length > 0) {
        translate(req.body.eng, {from: 'en', to: 'ru'}).then(re => {
            console.log(re.text);
            res.send(re.text);
        }).catch(err => {
            console.error(err);
        });
    }
};