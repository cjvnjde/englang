'use strict';
module.exports = function(app) {

    app.get('/', require('./games/words').get);

    require('./auth')(app);

    app.get('/addword', require('./add/addword').get);
    app.post('/addword', require('./add/addword').post);

    app.get('/adduserswords', require('../middleware/checkAuth'), require('./add/addusersword').get);
    app.post('/adduserswords', require('../middleware/checkAuth'), require('./add/addusersword').post);
    app.post('/deleteuserswords',
        require('../middleware/checkAuth'),
        require('./lib/deleteuserswords').post);
    app.get('/addpreposition', require('./add/addpreposition').get);
    app.post('/addpreposition', require('./add/addpreposition').post);

    app.get('/feedback', require('../middleware/checkAuth'), require('./pages/feedback').get);
    app.post('/feedback', require('../middleware/checkAuth'), require('./pages/feedback').post);
    app.get('/user', require('./auth/user').get);
    app.get('/irregularverbs/list', require('./pages/irregularverbslist').get);
    app.post('/irregularverbs/list', require('./pages/irregularverbslist').post);
    app.get('/prepositions', require('./pages/prepositions').get);
    app.post('/prepositions', require('./pages/prepositions').post);

    app.get('/personalgame',  require('../middleware/checkAuth'), require('./games/personalgame').get);
    app.post('/personalgame', require('../middleware/checkAuth'),  require('./games/personalgame').post);

    app.get('/addarticle',  require('../middleware/checkAuth'), require('../middleware/checkAdmin'), require('./add/addarticle').get);
    app.post('/addarticle',  require('../middleware/checkAuth'), require('../middleware/checkAdmin'), require('./add/addarticle').post);
    app.get('/articles', require('./articles/articles').get);
    app.post('/articles', require('./articles/articles').post);

    app.get('/addtext',  require('../middleware/checkAuth'), require('./add/addtext').get);
    app.post('/addtext',  require('../middleware/checkAuth'), require('./add/addtext').post);
    app.get('/texts', require('./pages/texts').get);
    app.post('/texts', require('./pages/texts').post);

    app.get('/texts/:id', require('./pages/text').get);

    //articles
    app.get('/articles/:id', require('./articles/article').get);
    app.get('/articles/edit/:id',
        require('../middleware/checkAuth'),
        require('../middleware/checkAdmin'),
        require('./articles/edit').get);

    app.post('/articles/edit',
        require('../middleware/checkAuth'),
        require('../middleware/checkAdmin'),
        require('./articles/edit').post);

    app.post('/articles/edit/:id',
        require('../middleware/checkAuth'),
        require('../middleware/checkAdmin'),
        require('./articles/edit').post);

    app.get('/articles/delete/:id',
        require('../middleware/checkAuth'),
        require('../middleware/checkAdmin'),
        require('./articles/delete').get);


    app.get('/adminpanel/users',
        require('../middleware/checkAuth'),
        require('../middleware/checkAdmin'),
        require('./adminpanel/users').get);

    app.post('/adminpanel/users',
        require('../middleware/checkAuth'),
        require('../middleware/checkAdmin'),
        require('./adminpanel/users').post);
    //new
    app.get('/games/words', require('./games/words').get);
    app.post('/games/words', require('./games/words').post);
    app.get('/games/irregularverbs', require('./games/irregularverbs').get);
    app.post('/games/irregularverbs', require('./games/irregularverbs').post);

    //app.get('/adminpanel', require('./adminpanel/adminpanel').get);
    app.get('/adminpanel', require('./adminpanel/adminarticles').get);
    app.get('/adminpanel/articles', require('./adminpanel/adminarticles').get);

    app.post('/translate/googletranslate', require('./translate/googletranslate').post);

};