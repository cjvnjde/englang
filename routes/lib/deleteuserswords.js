'use strict';
const User = require('../../models/user').User;

exports.post = function(req, res, next) {
    if(req.body.eng !== null && req.body.rus !== null){
        User.findOneAndUpdate({_id: req.user._id},
            {
                $pull: {
                    words: {
                        eng: req.body.eng,
                        rus: req.body.rus
                    }
                }
            },
            (err, data)=>{
                if(err) console.log(err);
                if(!err){
                    res.send('OK');
                }
            }
        );


    }
};
