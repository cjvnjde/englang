'use strict';
module.exports = function(app) {
    app.get('/signin', require('./auth/signin').get);
    app.post('/signin', require('./auth/signin').post);

    app.get('/signup', require('./auth/signup').get);
    app.post('/signup', require('./auth/signup').post);

    app.post('/logout', require('./auth/logout').post);
};