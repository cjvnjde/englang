'use strict';
const Preposition = require('../../models/preposition').Preposition;

exports.get = function(req, res, next) {
    res.render('games/prepositions', {title: 'Предлоги'});
    //console.log('There is some milk in the fridge.'[19]);
    //Preposition.add('There is some milk in the fridge.', 'in', 19);
};

exports.post = function(req, res, next) {
    Preposition.random(data => {
        res.send({
            phrase: data.phrase,
            preposition: data.preposition,
            prepositionIndex: data.prepositionIndex
        });
    });
};
