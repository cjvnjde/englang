'use strict';
const mail = require('../../mail/englangsender');
const Feedback = require('../../models/feedback').Feedback;
const request = require('request');

exports.get = function(req, res, next) {
    res.render('pages/feedback', { title: 'Обратная связь' });
};

exports.post = function(req, res, next) {
    if(req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null)
    {
        return res.json({'responseError' : 'Please select captcha first'});
    }
    const secretKey = '6LfiXUsUAAAAAHu5skbz0kvyYUK6Br3jS63dT7FP';
    const verificationURL = 'https://www.google.com/recaptcha/api/siteverify?secret=' + secretKey + '&response=' + req.body['g-recaptcha-response'] + '&remoteip=' + req.connection.remoteAddress;

    request(verificationURL,function(error,response,body) {
        body = JSON.parse(body);

        if(body.success !== undefined && !body.success) {
            return res.redirect('/feedback');
        }
        res.redirect('/feedback');
        Feedback.add(req.body.username, req.body.text);
        const mailoptions = {
            to: 'papera351@gmail.com',
            subject: `Feedback from ${req.body.username}`,
            text: req.body.text
        };
        mail.mail(mailoptions);

    });

};