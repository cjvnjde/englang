'use strict';
const Text = require('../../models/texts').Text;

exports.get = function(req, res, next) {
    console.log(req.params.id);
    Text.findOne({_id: req.params.id}, (err, data) => {
        if(err || data === '' || data === null) {
            res.render('pages/texts', {
                title: 'Список текстов'
            });
        }else {
            res.render('pages/textTranslater', {
                title: data.caption,
                text1: data.text
            });
        }
    });
};