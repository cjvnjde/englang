'use strict';
const Text = require('../../models/texts').Text;

exports.get = function(req, res, next) {
    //console.log(req.params.id);
    res.render('pages/texts', {
        title: 'Список статей'
    });
};


exports.post = function(req, res, next) {
    Text.find({}, {'user_id': true, 'caption': true}, (err, data) => {
        res.send(JSON.stringify(data));
    });
};
