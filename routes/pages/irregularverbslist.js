"use strict";
const IVerbs = require('../../models/irregularverbs').IVerbs

exports.get = function(req, res, next) {
    res.render('pages/irregularverbslist', { title: 'Список неправильных глаголов'});
};

exports.post = function(req, res, next) {
    IVerbs.find({}, (err, data) => {
        res.send(data);
    });
};
