'use strict';
const IVerbs = require('../../models/irregularverbs').IVerbs;

exports.get = function(req, res, next) {
    IVerbs.random(iv => {
        const neededForm = Math.round(Math.random())+2;
        let engWord = '';
        if(neededForm === 2){
            engWord = iv[0].pastSimple;
        }else{
            engWord = iv[0].pastParticiple;
        }
        res.render('games/irregularverbs', {
            title: 'Неправильные глаголы',
            rusVerb: iv[0].rus,
            infinitive: iv[0].infinitive,
            neededForm: neededForm.toString(),
            engWord: engWord
        });
    });

};

exports.post = function(req, res, next) {
    IVerbs.random((word)=>{
        res.send([
            {
                infinitive: word[0].infinitive,
                pastSimple: word[0].pastSimple,
                pastParticiple: word[0].pastParticiple,
                rus: word[0].rus
            },
            {
                infinitive: word[1].infinitive,
                pastSimple: word[1].pastSimple,
                pastParticiple: word[1].pastParticiple,
                rus: word[1].rus
            },
            {
                infinitive: word[2].infinitive,
                pastSimple: word[2].pastSimple,
                pastParticiple: word[2].pastParticiple,
                rus: word[2].rus
            }
        ]);
    });
};


