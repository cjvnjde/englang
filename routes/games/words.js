'use strict';
const Word = require('../../models/word').Word;
const config = require('config');


exports.get = function(req, res, next) {
    Word.random(word => {
        res.render('games/words', {
            title: 'Напишите слово правильно',
            rus: word[0].rus,
            eng: word[0].eng
        });
    });
};

exports.post = function(req, res, next) {
    Word.random((word)=>{
        res.send([
            {eng: word[0].eng,
                rus: word[0].rus},
            {eng: word[1].eng,
                rus: word[1].rus},
            {eng: word[2].eng,
                rus: word[2].rus}
        ]);
    });
};

