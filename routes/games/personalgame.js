'use strict';
const Word = require('../../models/word').Word;
const User = require('../../models/user').User;
const config = require('config');
const globals = require('globals');

exports.get = function(req, res, next) {
    const wordscount = req.user.words.length;
    if(wordscount !== 0) {
        const w = req.user.words[Math.round(Math.random() * wordscount)];
        res.render('games/personalgame', {
            title: 'Напишите слово правильно',
            rus: w.rus,
            eng: w.eng
        });
    }else{
        res.render('games/personalgame', {
            title: 'Напишите слово правильно',
            rus: 'Добавьте слова',
            eng: 'Add words'
        });
    }
};

exports.post = function(req, res, next) {
    const wordscount = req.user.words.length;
    console.log('POSTTT');
    if(wordscount !== 0){
        const w1 = req.user.words[Math.round(Math.random()*wordscount)];
        const w2 = req.user.words[Math.round(Math.random()*wordscount)];
        const w3 = req.user.words[Math.round(Math.random()*wordscount)];
        res.send([
            {rus: w2.rus,
                eng: w2.eng},
            {rus: w1.rus,
                eng: w1.eng},
            {rus: w3.rus,
                eng: w3.eng}
        ]);
    }else{
        res.send([
            {rus: 'Добавьте слова',
                eng: 'Add words'},
            {rus: 'Добавьте слова',
                eng: 'Add words'},
            {rus: 'Добавьте слова',
                eng: 'Add words'}]);
    }
};