'use strict';
const mongoose = require('mongoose');
const config = require('config');


mongoose.connect(config.get('mongoose.uri'), config.get('mongoose.options'))
    .catch(err => console.log('mongoose connect error' + err));


module.exports = mongoose;
