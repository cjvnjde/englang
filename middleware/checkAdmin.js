'use strict';
const HttpError = require('../error').HttpError;
const User = require('../models/user').User;

module.exports = function(req, res, next) {
    if (req.session.user) {
        User.findOne({_id: req.session.user}, {'accessRights': true}, (err, data) => {
            if(data.accessRights !== 1){
                return next(new HttpError(401, 'У вас нет доступа'));
            }
        });
    }
    next();
};