'use strict';
const addpreposition = (function() {
    return {
        init: function() {
            const phrase = document.getElementById('phrase');
            const preposition = document.getElementById('preposition');
            const index = document.getElementById('index');
            const button = document.getElementsByClassName('submit')[0];
            button.addEventListener('click', submitForm, false);
            function submitForm(event) {
                !event || event.preventDefault();
                const http = new XMLHttpRequest();
                http.open('POST', '/addpreposition', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

                const phrase2 = phrase.value.trim();
                const preposition2 = preposition.value.trim();
                const index2 = index.value.trim();

                http.send(`phrase=${phrase2} &preposition=${preposition2} &prepositionIndex=${index2}`);

            }

            preposition.onkeyup = function(){
                index.value = phrase.value.indexOf(preposition.value);
            };
        }
    };
})();
