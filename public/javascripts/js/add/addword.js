'use strict';
const addword = (function() {
    return {
        init: function() {
            const button = document.getElementsByClassName('submit')[0];
            button.addEventListener('click', submitForm, false);
            function submitForm(event) {
                !event || event.preventDefault();
                const http = new XMLHttpRequest();
                http.open('POST', '/addword', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

                const eng = document.getElementById('eng').value.trim();
                const rus = document.getElementById('rus').value.trim();

                const onlyRus = /[А-я]+/;
                const onlyEng = /[A-z]+/;

                if(eng != null && rus!= null) {
                    if (rus.match(onlyRus)) {
                        if (rus.match(onlyRus)[0].length !== rus.length) {
                            alert('not correct rus word');
                        } else {
                            if (eng.match(onlyEng)) {
                                if (eng.match(onlyEng)[0].length !== eng.length) {
                                    alert('not correct eng word');
                                } else {
                                    http.send(`eng=${eng} &rus=${rus}`);
                                }
                            } else {
                                alert('not correct eng word');
                            }
                        }
                    } else {
                        alert('not correct rus word');
                    }
                }

            }
        }
    };
})();