'use strict';
const addtext = (function() {
    return {
        init: function() {
            tinymce.init({
                selector: '#mytextarea',
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                    'save table contextmenu directionality emoticons template paste textcolor'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
            });

            document.querySelector('form').addEventListener('submit', function (e) {
                //console.log(tinymce.get('mytextarea').getContent());
                let caption = document.querySelector('input[type=\'text\']');
                //console.log(caption.value);
                sendData(caption.value, tinymce.get('mytextarea').getContent());
                tinyMCE.activeEditor.setContent('');
                caption.value = '';
                alert('OK');
                e.preventDefault();
                //console.log(e);
            }, false);

            function sendData(caption, data) {
                const http = new XMLHttpRequest();
                http.open('POST', '/addtext', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=UTF-8');
                //console.log(`data=${data} &caption=${caption}`);
                http.send(`data=${encodeURIComponent(data)} &caption=${encodeURIComponent(caption)}`);
            }
        }
    }
})();