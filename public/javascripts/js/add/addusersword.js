'use strict';
const adduserword = (function() {
    return {
        init: function() {
            const table = document.getElementById('userswords');
            const button = document.getElementsByClassName('submit')[0];
            button.addEventListener('click', submitForm, false);
            const http = new XMLHttpRequest();
            http.open('POST', '/adduserswords', true);
            http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            http.send(`eng=${''} &rus=${''}`);

            http.onreadystatechange = function () {

                if (this.readyState !== 4) return;
                if (this.status !== 200) return;

                if (http.responseText != null) {

                    while (table.firstChild) {
                        table.removeChild(table.firstChild);
                    }

                    const data = JSON.parse(http.responseText);
                    data.sort((a, b) => {
                        if(a.eng < b.eng) return -1;
                        else return 1;
                    });
                    generateTable(data);
                }
            };

            function generateTable(data){
                for(let i = 0; i < data.length; i++){
                    let tr = document.createElement('tr');
                    let eng = document.createElement('td');
                    let rus = document.createElement('td');
                    let del = document.createElement('td');

                    eng.appendChild(document.createTextNode(data[i].eng));
                    rus.appendChild(document.createTextNode(data[i].rus));

                    const a = document.createElement('a');
                    a.appendChild(document.createTextNode('delete'));
                    a.setAttribute('href', '#');
                    del.appendChild(a);

                    tr.appendChild(eng);
                    tr.appendChild(rus);
                    tr.appendChild(del);

                    table.appendChild(tr);
                }
            }

            table.addEventListener('click', deleteWord, false);
            function deleteWord(event) {
                if(event.target.closest('a') !== null){
                    let arr = event.target.closest('tr').innerText.split('\t');
                    deleteWordPost(arr[0], arr[1], event);
                }
            }

            function deleteWordPost(eng, rus, event){
                const http = new XMLHttpRequest();
                http.open('POST', '/deleteuserswords', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.send(`eng=${eng} &rus=${rus}`);
                http.onreadystatechange = function (){
                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;
                    if (http.responseText != null) {
                        event.target.closest('tr').style.visibility = 'collapse';
                    }
                };
            }

            function submitForm(event) {
                !event || event.preventDefault();
                const http = new XMLHttpRequest();
                http.open('POST', '/adduserswords', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

                const eng = document.getElementById('eng').value.trim();
                const rus = document.getElementById('rus').value.trim();

                const onlyRus = /[А-я ]+/;
                const onlyEng = /[A-z ]+/;

                http.onreadystatechange = function () {

                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;

                    if (http.responseText != null) {
                        document.getElementById('eng').value = '';
                        document.getElementById('rus').value = '';
                        while (table.firstChild) {
                            table.removeChild(table.firstChild);
                        }
                        const data = JSON.parse(http.responseText);
                        data.sort((a, b) => {
                            if (a.eng < b.eng) return -1;
                            else return 1;
                        });
                        generateTable(data);

                    }
                };

                if(eng != null && rus!= null) {
                    if (rus.match(onlyRus)) {
                        if (rus.match(onlyRus)[0].length !== rus.length) {
                            alert('not correct rus word');
                        } else {
                            if (eng.match(onlyEng)) {
                                if (eng.match(onlyEng)[0].length !== eng.length) {
                                    alert('not correct eng word');
                                } else {
                                    http.send(`eng=${eng} &rus=${rus}`);
                                }
                            } else {
                                alert('not correct eng word');
                            }
                        }
                    } else {
                        alert('not correct rus word');
                    }
                }

            }

        }
    };
})();

