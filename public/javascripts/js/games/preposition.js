'use strict';
const preposition = (function() {
    return {
        init: function() {
            const preposition = document.getElementById('prep');
            const prepositionText = document.getElementById('preposition');
            const button = document.getElementsByClassName('submit')[0];
            button.addEventListener('click', send, false);
            const lastPrepositions = document.getElementById('lastPrepositions');

            localStorage.setItem('preposition', 'test');
            send();

            function send(event){
                !event || event.preventDefault();
                const http = new XMLHttpRequest();
                http.open('POST', '/prepositions', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

                http.send(`preposition=${preposition.value}`);

                if(localStorage.getItem('preposition') !== 'test') {
                    updateLastPrepositions(preposition.value.trim(), localStorage.getItem('preposition'), localStorage.getItem('phrase'));
                }

                http.onreadystatechange = function () {
                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;

                    if (http.responseText != null) {
                        const data = JSON.parse(http.responseText);
                        let phrase = data.phrase.split('');
                        let preposition2 = data.preposition;
                        let prepositionIndex = data.prepositionIndex;

                        localStorage.setItem('preposition', preposition2);
                        localStorage.setItem('phrase', data.phrase);

                        phrase = mask(preposition2, phrase, prepositionIndex);

                        prepositionText.innerText = phrase.join('');
                        preposition.value = '';


                    }
                };
            }

            function mask(preposition, phrase, prepositionIndex) {
                let wordCount = preposition.length;
                const percent = 0.75;
                const wordMask = wordCount * percent;

                let random = Math.random();
                let masked = 0;

                for(let i = prepositionIndex; i < prepositionIndex+preposition.length; i++) {
                    //phrase[i] = '_';
                    if(wordCount < 3){
                        phrase[i] = '_';
                    }
                    if(random < percent && masked < wordMask && phrase[i] !== ' '){
                        phrase[i] = '_';
                        masked++;
                        random = Math.random();
                    }else{
                        random = Math.random()*random;
                    }
                }
                return phrase;
            }


            function updateLastPrepositions(userPreposition, correctPreposition, phrase) {
                if (userPreposition !== correctPreposition) {
                    const li = document.createElement('li');
                    li.appendChild(document.createTextNode(phrase + ' ::NOT:: ' + userPreposition));
                    li.className = 'redWord';
                    lastPrepositions.appendChild(li);
                } else {
                    const li = document.createElement('li');
                    li.appendChild(document.createTextNode(phrase));
                    li.className = 'greenWord';
                    lastPrepositions.appendChild(li);
                }
                pressOut();
            }

            function pressOut(){
                let child = lastPrepositions.childNodes;
                if(child.length>3){
                    lastPrepositions.removeChild(child[0]);
                }
            }
        }
    };
})();

