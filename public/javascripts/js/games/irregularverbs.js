/* eslint-disable no-undef */
'use strict';
const irregularverbs = (function() {
    const englang = App.init();
    return{
        init: function() {
            const rusWord = document.getElementsByClassName('rus-word')[0];//rusLabel
            const engWord = document.getElementsByClassName('eng-word')[0];//partOfEng
            const engWordForm = document.getElementsByClassName('eng-word')[1];//partOfEng
            const input = document.getElementsByClassName('form-control')[0];
            const lastWords = document.getElementsByClassName('last-words')[0];
            const nowEngD = document.getElementsByClassName('ivis')[0];

            const correct = document.getElementById('correct');
            const wrong = document.getElementById('wrong');
            const percent = document.getElementById('persent');
            const button = document.getElementsByClassName('submit')[0];

            button.addEventListener('click', send, false);

            let nowEng = nowEngD.innerText;
            nowEngD.parentNode.removeChild(nowEngD);
            send();
            function send(event){
                !event || event.preventDefault();
                englang.send('/games/irregularverbs', update);
            }

            function update(data){
                const eng = input;
                englang.updateLastWords({userWord: eng.value, correctWord: nowEng, rusWord: rusWord.innerText},lastWords);
                englang.updateStatistic({percent, wrong, correct});
                eng.value = '';
                const neededForm = Math.round(Math.random())+2;

                rusWord.innerText = data.rus;
                engWord.innerText = data.infinitive;

                if(neededForm === 2){
                    nowEng = data.pastSimple;
                    engWordForm.innerText = neededForm + ' форма. Past Simple';
                }else{
                    nowEng = data.pastParticiple;
                    engWordForm.innerText = neededForm + ' форма. Past Participle';
                }
            }

        }
    };
})();

