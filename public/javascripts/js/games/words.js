'use strict';
const words = (function() {
    const englang = App.init();
    return {
        init: function(){
            const rusWord = document.getElementsByClassName('rus-word')[0];//rusLabel
            const engWord = document.getElementsByClassName('eng-word')[0];//partOfEng
            const input = document.getElementsByClassName('form-control')[0];
            const lastWords = document.getElementsByClassName('last-words')[0];

            const correct = document.getElementById('correct');
            const wrong = document.getElementById('wrong');
            const percent = document.getElementById('persent');

            const button = document.getElementsByClassName('submit')[0];

            button.addEventListener('click', send, false);

            let nowEng = '';
            nowEng = engWord.innerText;

            engWord.innerText = englang.mask(engWord.innerText, 0.5);

            englang.split(engWord);
            send();
            function send(event){
                !event || event.preventDefault();
                englang.send('/games/words', update);
            }

            function update(data){
                const eng = input;
                englang.updateLastWords({userWord: eng.value, correctWord: nowEng, rusWord: rusWord.innerText},lastWords);
                englang.updateStatistic({percent, wrong, correct});
                eng.value = '';
                rusWord.innerText = data.rus;
                nowEng = data.eng;
                engWord.innerText = englang.mask(data.eng, 0.5);
                englang.split(engWord);
            }

            input.addEventListener('keyup', highlightWord, false);

            function highlightWord(event){
                englang.highlight(event, engWord);
            }
        }
    };
})();