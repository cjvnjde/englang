
const App = (function() {
    let data = [];
    let statistic = {
        wrong: 0,
        correct: 0
    };
    const loadData = function(url){
        const http = new XMLHttpRequest();
        http.open('POST', url, true);
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        http.send('');
        http.onreadystatechange = function () {
            if (this.readyState !== 4) return;
            if (this.status !== 200) return;
            if (http.responseText != null) {
                const response = JSON.parse(http.responseText);
                data.unshift(...response);
            }
        };
    };
    const send = function(url, callback) {
        if(data.length > 0){
            callback(data.pop());
        }
        if (data.length < 3) {
            loadData(url);
        }
    };
    const pressOut = function(element) {
        let child = element.childNodes;
        if (child.length > 3) {
            element.removeChild(child[0]);
        }
    };
    const updateLastWords = function(words, element) {
        let correctArr = words.correctWord.split('/');
        let check = false;

        for(let i = 0; i < correctArr.length; i++){
            check = words.userWord.toLowerCase().trim() === correctArr[i];
            if(check) break;
        }

        if (!check) {
            const li = document.createElement('li');
            li.appendChild(document.createTextNode(`${words.correctWord}: ${words.rusWord} not ${words.userWord}`));
            li.className = 'redWord';
            element.appendChild(li);
            statistic.wrong++;
        } else {
            const li = document.createElement('li');
            li.appendChild(document.createTextNode(`${words.correctWord}: ${words.rusWord}`));
            li.className = 'greenWord';
            element.appendChild(li);
            statistic.correct++;
        }
        pressOut(element);
    };
    const updateStatistic = function(elements) {
        elements.wrong.innerText = statistic.wrong + ' Неправильно';
        elements.correct.innerText = statistic.correct + ' Правильно';
        if((statistic.wrong+statistic.correct) !== 0) {
            elements.percent.innerText = Math.round( (statistic.correct * 100 / (statistic.wrong+statistic.correct)) * 100)/100 + ' % верно';
        }else{
            elements.percent.innerText = 100 +' % верно';
        }
    };

    const mask = function(word, percent) {
        let wordCount = word.length;
        let newWord = [];
        const wordMask = wordCount * percent;
        let i = 0;
        let masked = 0;
        let random = Math.random();
        if(wordCount <= 4){
            while (i < wordCount) {
                if (random < percent*1.5 && masked < wordMask && word[i] !== ' ') {
                    newWord.push('_');
                    masked++;
                    random = Math.random();
                } else {
                    random = Math.random() * random;
                    newWord.push(word[i]);
                }
                i++;
            }
        }else {
            while (i < wordCount) {
                if (random < percent && masked < wordMask && word[i] !== ' ') {
                    newWord.push('_');
                    masked++;
                    random = Math.random();
                } else {
                    random = Math.random() * random;
                    newWord.push(word[i]);
                }
                i++;
            }
        }
        return newWord.join('');
    };

    const split = function(element) {
        let engPart = element.innerText.split('');
        element.innerText = '';
        while(engPart.length > 0){
            const div = document.createElement('div');
            div.className = 'engPartclass';
            div.setAttribute('number', 1);
            let letter = engPart.shift();
            let text = document.createTextNode(letter);
            if(letter === ' '){
                div.classList.add('space');
            }
            div.appendChild(text);
            element.appendChild(div);
        }
    };

    const highlight = function(event, element){
        let leng = element.innerText.length;
        const leng2 = event.target.value.length;
        const elem = document.querySelectorAll(`[number="${1}"]`);
        let val = event.target.value.split('');

        for(let i = 0; i < leng; i++){
            if(i < leng2){
                if(elem[i].innerText.toLowerCase() !== val[i].toLowerCase() && elem[i].innerText !== '_'){
                    elem[i].style.background = '#FF5856';
                }else{
                    elem[i].style.background = '#2EFFE7';
                }
            }else{
                elem[i].style.background = 'none';
            }
        }
    };
    return {
        init: function() {
            return {
                send,
                updateStatistic,
                updateLastWords,
                mask,
                split,
                highlight
            };
        }
    };
})();