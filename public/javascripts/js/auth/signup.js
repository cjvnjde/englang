'use strict';
const signup = (function() {
    return {
        init: function() {
            function submitForm() {
                const http = new XMLHttpRequest();
                http.open('POST', '/signup', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                const login = document.getElementById('login').value;
                const password = document.getElementById('password').value;
                const email = document.getElementById('email').value;
                http.send(`login=${login} &password=${password} &email=${email}`);
            }
        }
    };
})();