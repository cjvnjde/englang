'use strict';
const signin = (function() {
    return {
        init: function() {
            function submitForm() {
                const http = new XMLHttpRequest();
                http.open('POST', '/signin', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                const login = document.getElementById('login').value;
                const password = document.getElementById('password').value;
                http.send(`login=${login} &password=${password}`);
            }
        }
    };
})();