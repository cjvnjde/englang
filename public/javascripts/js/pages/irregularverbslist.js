'use strict';
const irregularverbslist = (function() {
    return {
        init: function() {
            const table = document.getElementById('listofiv');
            const input = document.getElementsByClassName('search')[0];


            input.addEventListener('keyup', findWord, false);

            function findWord(){
                for(let i = 0; i < table.children.length; i++){
                    if(!table.children[i].innerHTML.includes(input.value)){
                        table.children[i].style.visibility = 'collapse';
                    }else{
                        table.children[i].style.visibility = 'visible';
                    }
                }
            }

            const http = new XMLHttpRequest();
            http.open('POST', '/irregularverbs/list', true);
            http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

            http.send();

            http.onreadystatechange = function () {

                if (this.readyState !== 4) return;
                if (this.status !== 200) return;

                if (http.responseText != null) {

                    const data = JSON.parse(http.responseText);
                    data.sort((a, b) => {
                        if(a.infinitive < b.infinitive) return -1;
                        else return 1;
                    });

                    for(let i = 0; i < data.length; i++){
                        let tr = document.createElement('tr');
                        let tdInfinitive = document.createElement('td');
                        let tdPastSimple = document.createElement('td');
                        let tdPastParticiple = document.createElement('td');
                        let tdRus = document.createElement('td');

                        tdInfinitive.appendChild(document.createTextNode(data[i].infinitive));
                        tdPastSimple.appendChild(document.createTextNode(data[i].pastSimple));
                        tdPastParticiple.appendChild(document.createTextNode(data[i].pastParticiple));
                        tdRus.appendChild(document.createTextNode(data[i].rus));
                        tr.appendChild(tdInfinitive);
                        tr.appendChild(tdPastSimple);
                        tr.appendChild(tdPastParticiple);
                        tr.appendChild(tdRus);

                        table.appendChild(tr);
                    }

                }
            };


        }
    };
})();
