'use strict';

const texts = (function () {
    return {
        init:   function() {
            const articlesContainer = document.getElementsByClassName('articles-container')[0];
            loadData();
            function loadData() {
                const http = new XMLHttpRequest();
                http.open('POST', '/texts', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.send('');
                http.onreadystatechange = function () {
                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;
                    if (http.responseText != null) {
                        const data = JSON.parse(http.responseText);
                        update(data);
                    }
                };
            }
            function update(data) {
                const ul = document.createElement('ul');
                for (let i = 0; i < data.length; i++) {
                    const li = document.createElement('li');
                    const a = document.createElement('a');
                    a.setAttribute('href','/texts/'+data[i]._id);
                    a.appendChild(document.createTextNode(data[i].caption));
                    li.appendChild(a);
                    ul.appendChild(li);
                }
                articlesContainer.appendChild(ul);
            }
        }
    };
})();