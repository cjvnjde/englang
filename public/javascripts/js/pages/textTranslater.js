'use strict';

const textTranslater = (function() {
    return {
        init: function() {
            const ru = document.getElementById('ru');
            const text = document.getElementById('text');
            const words = document.getElementById('words');
            const phrase = document.getElementById('phrase');
            const cloud = document.getElementById('cloud');
            const table = document.createElement('table');
            let textNo = '';
            words.appendChild(table);
            let wordsArr = [];
            let before = '';
            wrap(text);

            text.addEventListener('mouseup', getWordBySelection, false);

            function addWord(eng, rus, table){
                let tr = document.createElement('tr');
                let tdR = document.createElement('td');
                let tdE = document.createElement('td');
                tdR.appendChild(document.createTextNode(rus));
                tdE.appendChild(document.createTextNode(eng));
                tr.appendChild(tdE);
                tr.appendChild(tdR);
                table.appendChild(tr);
            }
            function splitWord(word){
                const letters = /^[a-zA-Z]+$/;
                if(!letters.test(word)){
                    let wordArray = word.split('');
                    let i = 0;
                    while(!letters.test(wordArray[i])){
                        wordArray.shift();
                        i++;
                    }
                    i = wordArray.length-1;
                    while(!letters.test(wordArray[i])){
                        wordArray.pop();
                        i--;
                    }
                    word = wordArray.join('');
                }
                return word;
            }
            function getWordBySelection(event){
                let span = event.target.closest('span');
                let selectedText = window.getSelection().toString().split(' ');
                let startWord = window.getSelection().anchorNode.data;
                let endWord = window.getSelection().focusNode.data;
                if(selectedText[0] !== startWord && selectedText[0] !== ''){
                    selectedText[0] = startWord;
                }
                if(selectedText[selectedText.length-1] !== endWord && selectedText[selectedText.length-1] !== ''){
                    selectedText[selectedText.length-1] = endWord;
                }
                if(selectedText.length <= 1){
                    selectedText[0] = startWord;
                }
                if(selectedText.length === 1){
                    phrase.style.visibility = 'hidden';
                    let eng = selectedText.join(' ');
                    eng = splitWord(eng);
                    translate(eng, rus => {
                        if(rus) {
                            if (span != null) {
                                cloud.style.left = (span.offsetLeft+30) + 'px';
                                cloud.style.top = (span.offsetTop+60) + 'px';
                                cloud.style.visibility = 'visible';
                                span.style.background = '#C9F6EC';
                            }

                            let res = wordsArr.find((element) => {
                                if (element === eng) {
                                    return element;
                                }
                            });
                            if (!res) {
                                wordsArr.push(eng);
                                addWord(eng, rus, table);
                            }
                            cloud.innerText = rus;
                        }
                    });
                }else{
                    translate(selectedText.join(' '), rus => {
                        phrase.style.visibility = 'visible';
                        cloud.style.visibility = 'hidden';
                        if(textNo){
                            phrase.removeChild(textNo);
                        }
                        let textNod = document.createTextNode(rus);
                        textNo = textNod;
                        phrase.appendChild(textNod);
                    });
                }
            }
            function getWordByClick(event){
                let span = event.target.closest('span');
                if(span !== null && before !== span.innerText){
                    before = span.innerText;
                    translate(before, rus => ru.textContent =  rus);
                }
            }
            function wrap(tag){
                const innerHtml = tag.innerHTML.split(' ');
                const reg1 = />/;
                const reg2 = /</;
                const reg_n = /[\n]/g;
                for(let i = 0; i < innerHtml.length; i++){
                    //console.log(innerHtml[i]);
                    if(innerHtml[i].match(reg1)){
                        let arr = innerHtml[i].split('>');
                        arr[1] =  '<span>'+arr[1]+'</span>';
                        innerHtml[i] = arr.join('>');
                    }
                    if(innerHtml[i].match(reg2)){
                        let arr = innerHtml[i].split('<');
                        arr[0] =  '<span>'+arr[0]+'</span>';
                        innerHtml[i] = arr.join('<');
                    }

                    if(innerHtml[i].match(reg_n)){
                        let arr = innerHtml[i].split('\n');
                        for(let j = 0; j < arr.length; j++){
                            arr[j] =  '<span>'+arr[j]+'</span>';
                        }
                        innerHtml[i] = arr.join('\n');
                    }else{
                        innerHtml[i] = '<span>'+innerHtml[i]+'</span>';
                    }
                }
                //console.log(innerHtml);
                tag.innerHTML = innerHtml.join(' ');
            }
            function translate(text, callback) {
                const http = new XMLHttpRequest();
                http.open('POST', '/translate/googletranslate', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.send(`eng=${text}`);

                http.onreadystatechange = function () {

                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;

                    if(http.responseText != null) {
                        callback(http.responseText);
                    }
                };
            }
        }
    };
})();
