const editarticle = (function() {
    return {
        init: function() {
            tinymce.init({
                selector: '#mytextarea',
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                    'save table contextmenu directionality emoticons template paste textcolor'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
            });
            const pp = document.getElementsByClassName('id')[0];
            const caption = document.querySelector('input[type=\'text\']');
            'use strict';

            downloadArticle();
            function downloadArticle() {
                const http = new XMLHttpRequest();
                http.open('POST', '/articles/edit/'+pp.innerText, true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.send();
                http.onreadystatechange = function () {
                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;
                    if (http.responseText != null) {
                        const data = JSON.parse(http.responseText);
                        tinymce.get('mytextarea').setContent(data.text1);
                        caption.value = data.title;
                    }
                };
            }

            document.querySelector('form').addEventListener('submit', function (e) {
                let caption = document.querySelector('input[type=\'text\']');
                sendData(caption.value, tinymce.get('mytextarea').getContent());
                tinymce.get('mytextarea').setContent('');
                caption.value = '';
                alert('OK');
                e.preventDefault();
            }, false);

            function sendData(caption, data) {
                const http = new XMLHttpRequest();
                http.open('POST', '/articles/edit', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=UTF-8');
                http.send(`id=${pp.innerText} &data=${encodeURIComponent(data)} &caption=${encodeURIComponent(caption)}`);
            }
        }
    };
})();