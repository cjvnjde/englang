const users = (function() {
    return {
        init: function(){
            const articlesContainer = document.getElementsByClassName('users')[0];
            loadData();
            function loadData() {
                const http = new XMLHttpRequest();
                http.open('POST', '/adminpanel/users', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.send('');
                http.onreadystatechange = function () {
                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;
                    if (http.responseText != null) {

                        const data = JSON.parse(http.responseText);
                        update(data);
                    }
                };
            }

            function update(data) {
                const table = document.createElement('table');
                table.className = 'adminpanel-table-articles';
                for (let i = 0; i < data.length; i++) {
                    const tr = document.createElement('tr');
                    const td = document.createElement('td');
                    td.appendChild(document.createTextNode(data[i].username));
                    tr.appendChild(td);


                    const tdAccess = document.createElement('td');
                    tdAccess.appendChild(document.createTextNode(data[i].accessRights));
                    tr.appendChild(tdAccess);

                    table.appendChild(tr);
                }
                articlesContainer.appendChild(table);
            }
        }
    };
})();