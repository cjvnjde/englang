const articles = (function() {
    return {
        init: function() {
            const articlesContainer = document.getElementsByClassName('articles-container')[0];
            loadData();

            function loadData() {
                const http = new XMLHttpRequest();
                http.open('POST', '/articles', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.send('');
                http.onreadystatechange = function () {
                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;
                    if (http.responseText != null) {
                        const data = JSON.parse(http.responseText);
                        update(data);
                    }
                };
            }

            function update(data) {
                const table = document.createElement('table');
                table.className = 'adminpanel-table-articles';
                for (let i = 0; i < data.length; i++) {
                    const tr = document.createElement('tr');

                    const td = document.createElement('td');
                    const a = document.createElement('a');
                    a.setAttribute('href', '/articles/' + data[i]._id);
                    a.appendChild(document.createTextNode(data[i].caption));
                    td.appendChild(a);

                    const tdUpdate = document.createElement('td');
                    const aUpdate = document.createElement('a');
                    aUpdate.setAttribute('href', '/articles/edit/' + data[i]._id);
                    aUpdate.appendChild(document.createTextNode('edit'));
                    tdUpdate.appendChild(aUpdate);

                    const tdDelete = document.createElement('td');
                    const aDelete = document.createElement('a');
                    aDelete.setAttribute('href', '/articles/edit/' + data[i]._id);
                    aDelete.appendChild(document.createTextNode('delete'));
                    tdDelete.appendChild(aDelete);

                    tr.appendChild(td);
                    tr.appendChild(tdUpdate);
                    tr.appendChild(tdDelete);

                    table.appendChild(tr);
                }
                articlesContainer.appendChild(table);
            }
        }
    };
})();