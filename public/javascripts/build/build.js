/*name - englang; version - 0.0.1; date - 2018-03-31*/

const App = (function() {
    let data = [];
    let statistic = {
        wrong: 0,
        correct: 0
    };
    const loadData = function(url){
        const http = new XMLHttpRequest();
        http.open('POST', url, true);
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        http.send('');
        http.onreadystatechange = function () {
            if (this.readyState !== 4) return;
            if (this.status !== 200) return;
            if (http.responseText != null) {
                const response = JSON.parse(http.responseText);
                data.unshift(...response);
            }
        };
    };
    const send = function(url, callback) {
        if(data.length > 0){
            callback(data.pop());
        }
        if (data.length < 3) {
            loadData(url);
        }
    };
    const pressOut = function(element) {
        let child = element.childNodes;
        if (child.length > 3) {
            element.removeChild(child[0]);
        }
    };
    const updateLastWords = function(words, element) {
        let correctArr = words.correctWord.split('/');
        let check = false;

        for(let i = 0; i < correctArr.length; i++){
            check = words.userWord.toLowerCase().trim() === correctArr[i];
            if(check) break;
        }

        if (!check) {
            const li = document.createElement('li');
            li.appendChild(document.createTextNode(`${words.correctWord}: ${words.rusWord} not ${words.userWord}`));
            li.className = 'redWord';
            element.appendChild(li);
            statistic.wrong++;
        } else {
            const li = document.createElement('li');
            li.appendChild(document.createTextNode(`${words.correctWord}: ${words.rusWord}`));
            li.className = 'greenWord';
            element.appendChild(li);
            statistic.correct++;
        }
        pressOut(element);
    };
    const updateStatistic = function(elements) {
        elements.wrong.innerText = statistic.wrong + ' Неправильно';
        elements.correct.innerText = statistic.correct + ' Правильно';
        if((statistic.wrong+statistic.correct) !== 0) {
            elements.percent.innerText = Math.round( (statistic.correct * 100 / (statistic.wrong+statistic.correct)) * 100)/100 + ' % верно';
        }else{
            elements.percent.innerText = 100 +' % верно';
        }
    };

    const mask = function(word, percent) {
        let wordCount = word.length;
        let newWord = [];
        const wordMask = wordCount * percent;
        let i = 0;
        let masked = 0;
        let random = Math.random();
        if(wordCount <= 4){
            while (i < wordCount) {
                if (random < percent*1.5 && masked < wordMask && word[i] !== ' ') {
                    newWord.push('_');
                    masked++;
                    random = Math.random();
                } else {
                    random = Math.random() * random;
                    newWord.push(word[i]);
                }
                i++;
            }
        }else {
            while (i < wordCount) {
                if (random < percent && masked < wordMask && word[i] !== ' ') {
                    newWord.push('_');
                    masked++;
                    random = Math.random();
                } else {
                    random = Math.random() * random;
                    newWord.push(word[i]);
                }
                i++;
            }
        }
        return newWord.join('');
    };

    const split = function(element) {
        let engPart = element.innerText.split('');
        element.innerText = '';
        while(engPart.length > 0){
            const div = document.createElement('div');
            div.className = 'engPartclass';
            div.setAttribute('number', 1);
            let letter = engPart.shift();
            let text = document.createTextNode(letter);
            if(letter === ' '){
                div.classList.add('space');
            }
            div.appendChild(text);
            element.appendChild(div);
        }
    };

    const highlight = function(event, element){
        let leng = element.innerText.length;
        const leng2 = event.target.value.length;
        const elem = document.querySelectorAll(`[number="${1}"]`);
        let val = event.target.value.split('');

        for(let i = 0; i < leng; i++){
            if(i < leng2){
                if(elem[i].innerText.toLowerCase() !== val[i].toLowerCase() && elem[i].innerText !== '_'){
                    elem[i].style.background = '#FF5856';
                }else{
                    elem[i].style.background = '#2EFFE7';
                }
            }else{
                elem[i].style.background = 'none';
            }
        }
    };
    return {
        init: function() {
            return {
                send,
                updateStatistic,
                updateLastWords,
                mask,
                split,
                highlight
            };
        }
    };
})();
/* eslint-disable no-undef */
'use strict';
const irregularverbs = (function() {
    const englang = App.init();
    return{
        init: function() {
            const rusWord = document.getElementsByClassName('rus-word')[0];//rusLabel
            const engWord = document.getElementsByClassName('eng-word')[0];//partOfEng
            const engWordForm = document.getElementsByClassName('eng-word')[1];//partOfEng
            const input = document.getElementsByClassName('form-control')[0];
            const lastWords = document.getElementsByClassName('last-words')[0];
            const nowEngD = document.getElementsByClassName('ivis')[0];

            const correct = document.getElementById('correct');
            const wrong = document.getElementById('wrong');
            const percent = document.getElementById('persent');
            const button = document.getElementsByClassName('submit')[0];

            button.addEventListener('click', send, false);

            let nowEng = nowEngD.innerText;
            nowEngD.parentNode.removeChild(nowEngD);
            send();
            function send(event){
                !event || event.preventDefault();
                englang.send('/games/irregularverbs', update);
            }

            function update(data){
                const eng = input;
                englang.updateLastWords({userWord: eng.value, correctWord: nowEng, rusWord: rusWord.innerText},lastWords);
                englang.updateStatistic({percent, wrong, correct});
                eng.value = '';
                const neededForm = Math.round(Math.random())+2;

                rusWord.innerText = data.rus;
                engWord.innerText = data.infinitive;

                if(neededForm === 2){
                    nowEng = data.pastSimple;
                    engWordForm.innerText = neededForm + ' форма. Past Simple';
                }else{
                    nowEng = data.pastParticiple;
                    engWordForm.innerText = neededForm + ' форма. Past Participle';
                }
            }

        }
    };
})();


'use strict';
const personalwords = (function() {
    const englang = App.init();
    return {
        init: function() {
            const rusWord = document.getElementsByClassName('rus-word')[0];//rusLabel
            const engWord = document.getElementsByClassName('eng-word')[0];//partOfEng
            const input = document.getElementsByClassName('form-control')[0];
            const lastWords = document.getElementsByClassName('last-words')[0];

            const correct = document.getElementById('correct');
            const wrong = document.getElementById('wrong');
            const percent = document.getElementById('persent');
            const button = document.getElementsByClassName('submit')[0];

            button.addEventListener('click', send, false);

            let nowEng = '';
            nowEng = engWord.innerText;

            engWord.innerText = englang.mask(engWord.innerText, 0.5);

            englang.split(engWord);

            send();

            function send(event){
                !event || event.preventDefault();
                englang.send('/personalgame', update);
            }
            function update(data){
                const eng = input;
                //updateLastWords(eng.value, nowEng, rusWord.innerText);
                englang.updateLastWords({userWord: eng.value, correctWord: nowEng, rusWord: rusWord.innerText},lastWords);
                englang.updateStatistic({percent, wrong, correct});
                eng.value = '';
                rusWord.innerText = data.rus;
                nowEng = data.eng;
                engWord.innerText = englang.mask(data.eng, 0.5);
                englang.split(engWord);
            }

            input.addEventListener('keyup', highlightWord, false);

            function highlightWord(event){
                englang.highlight(event, engWord);
            }
        }
    };
})();


'use strict';
const preposition = (function() {
    return {
        init: function() {
            const preposition = document.getElementById('prep');
            const prepositionText = document.getElementById('preposition');
            const button = document.getElementsByClassName('submit')[0];
            button.addEventListener('click', send, false);
            const lastPrepositions = document.getElementById('lastPrepositions');

            localStorage.setItem('preposition', 'test');
            send();

            function send(event){
                !event || event.preventDefault();
                const http = new XMLHttpRequest();
                http.open('POST', '/prepositions', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

                http.send(`preposition=${preposition.value}`);

                if(localStorage.getItem('preposition') !== 'test') {
                    updateLastPrepositions(preposition.value.trim(), localStorage.getItem('preposition'), localStorage.getItem('phrase'));
                }

                http.onreadystatechange = function () {
                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;

                    if (http.responseText != null) {
                        const data = JSON.parse(http.responseText);
                        let phrase = data.phrase.split('');
                        let preposition2 = data.preposition;
                        let prepositionIndex = data.prepositionIndex;

                        localStorage.setItem('preposition', preposition2);
                        localStorage.setItem('phrase', data.phrase);

                        phrase = mask(preposition2, phrase, prepositionIndex);

                        prepositionText.innerText = phrase.join('');
                        preposition.value = '';


                    }
                };
            }

            function mask(preposition, phrase, prepositionIndex) {
                let wordCount = preposition.length;
                const percent = 0.75;
                const wordMask = wordCount * percent;

                let random = Math.random();
                let masked = 0;

                for(let i = prepositionIndex; i < prepositionIndex+preposition.length; i++) {
                    //phrase[i] = '_';
                    if(wordCount < 3){
                        phrase[i] = '_';
                    }
                    if(random < percent && masked < wordMask && phrase[i] !== ' '){
                        phrase[i] = '_';
                        masked++;
                        random = Math.random();
                    }else{
                        random = Math.random()*random;
                    }
                }
                return phrase;
            }


            function updateLastPrepositions(userPreposition, correctPreposition, phrase) {
                if (userPreposition !== correctPreposition) {
                    const li = document.createElement('li');
                    li.appendChild(document.createTextNode(phrase + ' ::NOT:: ' + userPreposition));
                    li.className = 'redWord';
                    lastPrepositions.appendChild(li);
                } else {
                    const li = document.createElement('li');
                    li.appendChild(document.createTextNode(phrase));
                    li.className = 'greenWord';
                    lastPrepositions.appendChild(li);
                }
                pressOut();
            }

            function pressOut(){
                let child = lastPrepositions.childNodes;
                if(child.length>3){
                    lastPrepositions.removeChild(child[0]);
                }
            }
        }
    };
})();


'use strict';
const words = (function() {
    const englang = App.init();
    return {
        init: function(){
            const rusWord = document.getElementsByClassName('rus-word')[0];//rusLabel
            const engWord = document.getElementsByClassName('eng-word')[0];//partOfEng
            const input = document.getElementsByClassName('form-control')[0];
            const lastWords = document.getElementsByClassName('last-words')[0];

            const correct = document.getElementById('correct');
            const wrong = document.getElementById('wrong');
            const percent = document.getElementById('persent');

            const button = document.getElementsByClassName('submit')[0];

            button.addEventListener('click', send, false);

            let nowEng = '';
            nowEng = engWord.innerText;

            engWord.innerText = englang.mask(engWord.innerText, 0.5);

            englang.split(engWord);
            send();
            function send(event){
                !event || event.preventDefault();
                englang.send('/games/words', update);
            }

            function update(data){
                const eng = input;
                englang.updateLastWords({userWord: eng.value, correctWord: nowEng, rusWord: rusWord.innerText},lastWords);
                englang.updateStatistic({percent, wrong, correct});
                eng.value = '';
                rusWord.innerText = data.rus;
                nowEng = data.eng;
                engWord.innerText = englang.mask(data.eng, 0.5);
                englang.split(engWord);
            }

            input.addEventListener('keyup', highlightWord, false);

            function highlightWord(event){
                englang.highlight(event, engWord);
            }
        }
    };
})();
const articles = (function() {
    return {
        init: function() {
            const articlesContainer = document.getElementsByClassName('articles-container')[0];
            loadData();

            function loadData() {
                const http = new XMLHttpRequest();
                http.open('POST', '/articles', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.send('');
                http.onreadystatechange = function () {
                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;
                    if (http.responseText != null) {
                        const data = JSON.parse(http.responseText);
                        update(data);
                    }
                };
            }

            function update(data) {
                const table = document.createElement('table');
                table.className = 'adminpanel-table-articles';
                for (let i = 0; i < data.length; i++) {
                    const tr = document.createElement('tr');

                    const td = document.createElement('td');
                    const a = document.createElement('a');
                    a.setAttribute('href', '/articles/' + data[i]._id);
                    a.appendChild(document.createTextNode(data[i].caption));
                    td.appendChild(a);

                    const tdUpdate = document.createElement('td');
                    const aUpdate = document.createElement('a');
                    aUpdate.setAttribute('href', '/articles/edit/' + data[i]._id);
                    aUpdate.appendChild(document.createTextNode('edit'));
                    tdUpdate.appendChild(aUpdate);

                    const tdDelete = document.createElement('td');
                    const aDelete = document.createElement('a');
                    aDelete.setAttribute('href', '/articles/edit/' + data[i]._id);
                    aDelete.appendChild(document.createTextNode('delete'));
                    tdDelete.appendChild(aDelete);

                    tr.appendChild(td);
                    tr.appendChild(tdUpdate);
                    tr.appendChild(tdDelete);

                    table.appendChild(tr);
                }
                articlesContainer.appendChild(table);
            }
        }
    };
})();
const editarticle = (function() {
    return {
        init: function() {
            tinymce.init({
                selector: '#mytextarea',
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                    'save table contextmenu directionality emoticons template paste textcolor'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
            });
            const pp = document.getElementsByClassName('id')[0];
            const caption = document.querySelector('input[type=\'text\']');
            'use strict';

            downloadArticle();
            function downloadArticle() {
                const http = new XMLHttpRequest();
                http.open('POST', '/articles/edit/'+pp.innerText, true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.send();
                http.onreadystatechange = function () {
                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;
                    if (http.responseText != null) {
                        const data = JSON.parse(http.responseText);
                        tinymce.get('mytextarea').setContent(data.text1);
                        caption.value = data.title;
                    }
                };
            }

            document.querySelector('form').addEventListener('submit', function (e) {
                let caption = document.querySelector('input[type=\'text\']');
                sendData(caption.value, tinymce.get('mytextarea').getContent());
                tinymce.get('mytextarea').setContent('');
                caption.value = '';
                alert('OK');
                e.preventDefault();
            }, false);

            function sendData(caption, data) {
                const http = new XMLHttpRequest();
                http.open('POST', '/articles/edit', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=UTF-8');
                http.send(`id=${pp.innerText} &data=${encodeURIComponent(data)} &caption=${encodeURIComponent(caption)}`);
            }
        }
    };
})();
const users = (function() {
    return {
        init: function(){
            const articlesContainer = document.getElementsByClassName('users')[0];
            loadData();
            function loadData() {
                const http = new XMLHttpRequest();
                http.open('POST', '/adminpanel/users', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.send('');
                http.onreadystatechange = function () {
                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;
                    if (http.responseText != null) {

                        const data = JSON.parse(http.responseText);
                        update(data);
                    }
                };
            }

            function update(data) {
                const table = document.createElement('table');
                table.className = 'adminpanel-table-articles';
                for (let i = 0; i < data.length; i++) {
                    const tr = document.createElement('tr');
                    const td = document.createElement('td');
                    td.appendChild(document.createTextNode(data[i].username));
                    tr.appendChild(td);


                    const tdAccess = document.createElement('td');
                    tdAccess.appendChild(document.createTextNode(data[i].accessRights));
                    tr.appendChild(tdAccess);

                    table.appendChild(tr);
                }
                articlesContainer.appendChild(table);
            }
        }
    };
})();
'use strict';
const header = (function() {
    return {
        init: function() {
            let headerUser = document.getElementsByClassName('header-content')[0];
            const logoutEl = document.getElementsByClassName('logout')[0];
            logoutEl.addEventListener('click', logout, true);

            headerUser.addEventListener('click', aler, true);

            function aler(event) {
                if(event.target.closest('li') != null)
                {
                    if(event.target.closest('li').classList.contains('dropdown')){
                        if (event.target.closest('li').classList.contains('dropdown') && event.target.closest('li').classList.contains('open')) {
                            event.target.closest('li').classList.remove('open');
                        }else{
                            event.target.closest('li').classList.add('open');
                        }
                    }

                }
            }

            function logout() {
                const http = new XMLHttpRequest();
                http.open('POST', '/logout', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.send('login=-1 &password=-1');
                http.onreadystatechange = function () {
                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;
                    window.location = '/';
                };
            }
        }
    };
})();
'use strict';
const signin = (function() {
    return {
        init: function() {
            function submitForm() {
                const http = new XMLHttpRequest();
                http.open('POST', '/signin', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                const login = document.getElementById('login').value;
                const password = document.getElementById('password').value;
                http.send(`login=${login} &password=${password}`);
            }
        }
    };
})();
'use strict';
const signup = (function() {
    return {
        init: function() {
            function submitForm() {
                const http = new XMLHttpRequest();
                http.open('POST', '/signup', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                const login = document.getElementById('login').value;
                const password = document.getElementById('password').value;
                const email = document.getElementById('email').value;
                http.send(`login=${login} &password=${password} &email=${email}`);
            }
        }
    };
})();
'use strict';

const articles2 = (function() {
    return {
        init: function() {
            const articlesContainer = document.getElementsByClassName('articles-container')[0];
            loadData();
            function loadData() {
                const http = new XMLHttpRequest();
                http.open('POST', '/articles', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.send('');
                http.onreadystatechange = function () {
                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;
                    if (http.responseText != null) {
                        const data = JSON.parse(http.responseText);
                        update(data);
                    }
                };
            }
            function update(data) {
                const ul = document.createElement('ul');
                for (let i = 0; i < data.length; i++) {
                    const li = document.createElement('li');
                    const a = document.createElement('a');
                    a.setAttribute('href','/articles/'+data[i]._id);
                    a.appendChild(document.createTextNode(data[i].caption));
                    li.appendChild(a);
                    ul.appendChild(li);
                }
                articlesContainer.appendChild(ul);
            }
        }
    };
})();
'use strict';
const irregularverbslist = (function() {
    return {
        init: function() {
            const table = document.getElementById('listofiv');
            const input = document.getElementsByClassName('search')[0];


            input.addEventListener('keyup', findWord, false);

            function findWord(){
                for(let i = 0; i < table.children.length; i++){
                    if(!table.children[i].innerHTML.includes(input.value)){
                        table.children[i].style.visibility = 'collapse';
                    }else{
                        table.children[i].style.visibility = 'visible';
                    }
                }
            }

            const http = new XMLHttpRequest();
            http.open('POST', '/irregularverbs/list', true);
            http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

            http.send();

            http.onreadystatechange = function () {

                if (this.readyState !== 4) return;
                if (this.status !== 200) return;

                if (http.responseText != null) {

                    const data = JSON.parse(http.responseText);
                    data.sort((a, b) => {
                        if(a.infinitive < b.infinitive) return -1;
                        else return 1;
                    });

                    for(let i = 0; i < data.length; i++){
                        let tr = document.createElement('tr');
                        let tdInfinitive = document.createElement('td');
                        let tdPastSimple = document.createElement('td');
                        let tdPastParticiple = document.createElement('td');
                        let tdRus = document.createElement('td');

                        tdInfinitive.appendChild(document.createTextNode(data[i].infinitive));
                        tdPastSimple.appendChild(document.createTextNode(data[i].pastSimple));
                        tdPastParticiple.appendChild(document.createTextNode(data[i].pastParticiple));
                        tdRus.appendChild(document.createTextNode(data[i].rus));
                        tr.appendChild(tdInfinitive);
                        tr.appendChild(tdPastSimple);
                        tr.appendChild(tdPastParticiple);
                        tr.appendChild(tdRus);

                        table.appendChild(tr);
                    }

                }
            };


        }
    };
})();

'use strict';

const texts = (function () {
    return {
        init:   function() {
            const articlesContainer = document.getElementsByClassName('articles-container')[0];
            loadData();
            function loadData() {
                const http = new XMLHttpRequest();
                http.open('POST', '/texts', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.send('');
                http.onreadystatechange = function () {
                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;
                    if (http.responseText != null) {
                        const data = JSON.parse(http.responseText);
                        update(data);
                    }
                };
            }
            function update(data) {
                const ul = document.createElement('ul');
                for (let i = 0; i < data.length; i++) {
                    const li = document.createElement('li');
                    const a = document.createElement('a');
                    a.setAttribute('href','/texts/'+data[i]._id);
                    a.appendChild(document.createTextNode(data[i].caption));
                    li.appendChild(a);
                    ul.appendChild(li);
                }
                articlesContainer.appendChild(ul);
            }
        }
    };
})();
'use strict';

const textTranslater = (function() {
    return {
        init: function() {
            const ru = document.getElementById('ru');
            const text = document.getElementById('text');
            const words = document.getElementById('words');
            const phrase = document.getElementById('phrase');
            const cloud = document.getElementById('cloud');
            const table = document.createElement('table');
            let textNo = '';
            words.appendChild(table);
            let wordsArr = [];
            let before = '';
            wrap(text);

            text.addEventListener('mouseup', getWordBySelection, false);

            function addWord(eng, rus, table){
                let tr = document.createElement('tr');
                let tdR = document.createElement('td');
                let tdE = document.createElement('td');
                tdR.appendChild(document.createTextNode(rus));
                tdE.appendChild(document.createTextNode(eng));
                tr.appendChild(tdE);
                tr.appendChild(tdR);
                table.appendChild(tr);
            }
            function splitWord(word){
                const letters = /^[a-zA-Z]+$/;
                if(!letters.test(word)){
                    let wordArray = word.split('');
                    let i = 0;
                    while(!letters.test(wordArray[i])){
                        wordArray.shift();
                        i++;
                    }
                    i = wordArray.length-1;
                    while(!letters.test(wordArray[i])){
                        wordArray.pop();
                        i--;
                    }
                    word = wordArray.join('');
                }
                return word;
            }
            function getWordBySelection(event){
                let span = event.target.closest('span');
                let selectedText = window.getSelection().toString().split(' ');
                let startWord = window.getSelection().anchorNode.data;
                let endWord = window.getSelection().focusNode.data;
                if(selectedText[0] !== startWord && selectedText[0] !== ''){
                    selectedText[0] = startWord;
                }
                if(selectedText[selectedText.length-1] !== endWord && selectedText[selectedText.length-1] !== ''){
                    selectedText[selectedText.length-1] = endWord;
                }
                if(selectedText.length <= 1){
                    selectedText[0] = startWord;
                }
                if(selectedText.length === 1){
                    phrase.style.visibility = 'hidden';
                    let eng = selectedText.join(' ');
                    eng = splitWord(eng);
                    translate(eng, rus => {
                        if(rus) {
                            if (span != null) {
                                cloud.style.left = (span.offsetLeft+30) + 'px';
                                cloud.style.top = (span.offsetTop+60) + 'px';
                                cloud.style.visibility = 'visible';
                                span.style.background = '#C9F6EC';
                            }

                            let res = wordsArr.find((element) => {
                                if (element === eng) {
                                    return element;
                                }
                            });
                            if (!res) {
                                wordsArr.push(eng);
                                addWord(eng, rus, table);
                            }
                            cloud.innerText = rus;
                        }
                    });
                }else{
                    translate(selectedText.join(' '), rus => {
                        phrase.style.visibility = 'visible';
                        cloud.style.visibility = 'hidden';
                        if(textNo){
                            phrase.removeChild(textNo);
                        }
                        let textNod = document.createTextNode(rus);
                        textNo = textNod;
                        phrase.appendChild(textNod);
                    });
                }
            }
            function getWordByClick(event){
                let span = event.target.closest('span');
                if(span !== null && before !== span.innerText){
                    before = span.innerText;
                    translate(before, rus => ru.textContent =  rus);
                }
            }
            function wrap(tag){
                const innerHtml = tag.innerHTML.split(' ');
                const reg1 = />/;
                const reg2 = /</;
                const reg_n = /[\n]/g;
                for(let i = 0; i < innerHtml.length; i++){
                    //console.log(innerHtml[i]);
                    if(innerHtml[i].match(reg1)){
                        let arr = innerHtml[i].split('>');
                        arr[1] =  '<span>'+arr[1]+'</span>';
                        innerHtml[i] = arr.join('>');
                    }
                    if(innerHtml[i].match(reg2)){
                        let arr = innerHtml[i].split('<');
                        arr[0] =  '<span>'+arr[0]+'</span>';
                        innerHtml[i] = arr.join('<');
                    }

                    if(innerHtml[i].match(reg_n)){
                        let arr = innerHtml[i].split('\n');
                        for(let j = 0; j < arr.length; j++){
                            arr[j] =  '<span>'+arr[j]+'</span>';
                        }
                        innerHtml[i] = arr.join('\n');
                    }else{
                        innerHtml[i] = '<span>'+innerHtml[i]+'</span>';
                    }
                }
                //console.log(innerHtml);
                tag.innerHTML = innerHtml.join(' ');
            }
            function translate(text, callback) {
                const http = new XMLHttpRequest();
                http.open('POST', '/translate/googletranslate', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.send(`eng=${text}`);

                http.onreadystatechange = function () {

                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;

                    if(http.responseText != null) {
                        callback(http.responseText);
                    }
                };
            }
        }
    };
})();

'use strict';
const addarticle = (function() {
    return {
        init: function() {
            tinymce.init({
                selector: '#mytextarea',
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                    'save table contextmenu directionality emoticons template paste textcolor'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
            });
            document.querySelector('form').addEventListener('submit', function (e) {
                //console.log(tinymce.get('mytextarea').getContent());
                let caption = document.querySelector('input[type=\'text\']');
                //console.log(caption.value);
                sendData(caption.value, tinymce.get('mytextarea').getContent());
                tinyMCE.activeEditor.setContent('');
                caption.value = '';
                alert('OK');
                e.preventDefault();
            }, false);
            function sendData(caption, data) {
                const http = new XMLHttpRequest();
                http.open('POST', '/addarticle', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=UTF-8');
                //console.log(`data=${data} &caption=${caption}`);
                http.send(`data=${encodeURIComponent(data)} &caption=${encodeURIComponent(caption)}`);
            }
        }
    };
})();
'use strict';
const addpreposition = (function() {
    return {
        init: function() {
            const phrase = document.getElementById('phrase');
            const preposition = document.getElementById('preposition');
            const index = document.getElementById('index');
            const button = document.getElementsByClassName('submit')[0];
            button.addEventListener('click', submitForm, false);
            function submitForm(event) {
                !event || event.preventDefault();
                const http = new XMLHttpRequest();
                http.open('POST', '/addpreposition', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

                const phrase2 = phrase.value.trim();
                const preposition2 = preposition.value.trim();
                const index2 = index.value.trim();

                http.send(`phrase=${phrase2} &preposition=${preposition2} &prepositionIndex=${index2}`);

            }

            preposition.onkeyup = function(){
                index.value = phrase.value.indexOf(preposition.value);
            };
        }
    };
})();

'use strict';
const addtext = (function() {
    return {
        init: function() {
            tinymce.init({
                selector: '#mytextarea',
                plugins: [
                    'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
                    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
                    'save table contextmenu directionality emoticons template paste textcolor'
                ],
                toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
            });

            document.querySelector('form').addEventListener('submit', function (e) {
                //console.log(tinymce.get('mytextarea').getContent());
                let caption = document.querySelector('input[type=\'text\']');
                //console.log(caption.value);
                sendData(caption.value, tinymce.get('mytextarea').getContent());
                tinyMCE.activeEditor.setContent('');
                caption.value = '';
                alert('OK');
                e.preventDefault();
                //console.log(e);
            }, false);

            function sendData(caption, data) {
                const http = new XMLHttpRequest();
                http.open('POST', '/addtext', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=UTF-8');
                //console.log(`data=${data} &caption=${caption}`);
                http.send(`data=${encodeURIComponent(data)} &caption=${encodeURIComponent(caption)}`);
            }
        }
    }
})();
'use strict';
const adduserword = (function() {
    return {
        init: function() {
            const table = document.getElementById('userswords');
            const button = document.getElementsByClassName('submit')[0];
            button.addEventListener('click', submitForm, false);
            const http = new XMLHttpRequest();
            http.open('POST', '/adduserswords', true);
            http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            http.send(`eng=${''} &rus=${''}`);

            http.onreadystatechange = function () {

                if (this.readyState !== 4) return;
                if (this.status !== 200) return;

                if (http.responseText != null) {

                    while (table.firstChild) {
                        table.removeChild(table.firstChild);
                    }

                    const data = JSON.parse(http.responseText);
                    data.sort((a, b) => {
                        if(a.eng < b.eng) return -1;
                        else return 1;
                    });
                    generateTable(data);
                }
            };

            function generateTable(data){
                for(let i = 0; i < data.length; i++){
                    let tr = document.createElement('tr');
                    let eng = document.createElement('td');
                    let rus = document.createElement('td');
                    let del = document.createElement('td');

                    eng.appendChild(document.createTextNode(data[i].eng));
                    rus.appendChild(document.createTextNode(data[i].rus));

                    const a = document.createElement('a');
                    a.appendChild(document.createTextNode('delete'));
                    a.setAttribute('href', '#');
                    del.appendChild(a);

                    tr.appendChild(eng);
                    tr.appendChild(rus);
                    tr.appendChild(del);

                    table.appendChild(tr);
                }
            }

            table.addEventListener('click', deleteWord, false);
            function deleteWord(event) {
                if(event.target.closest('a') !== null){
                    let arr = event.target.closest('tr').innerText.split('\t');
                    deleteWordPost(arr[0], arr[1], event);
                }
            }

            function deleteWordPost(eng, rus, event){
                const http = new XMLHttpRequest();
                http.open('POST', '/deleteuserswords', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                http.send(`eng=${eng} &rus=${rus}`);
                http.onreadystatechange = function (){
                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;
                    if (http.responseText != null) {
                        event.target.closest('tr').style.visibility = 'collapse';
                    }
                };
            }

            function submitForm(event) {
                !event || event.preventDefault();
                const http = new XMLHttpRequest();
                http.open('POST', '/adduserswords', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

                const eng = document.getElementById('eng').value.trim();
                const rus = document.getElementById('rus').value.trim();

                const onlyRus = /[А-я ]+/;
                const onlyEng = /[A-z ]+/;

                http.onreadystatechange = function () {

                    if (this.readyState !== 4) return;
                    if (this.status !== 200) return;

                    if (http.responseText != null) {
                        document.getElementById('eng').value = '';
                        document.getElementById('rus').value = '';
                        while (table.firstChild) {
                            table.removeChild(table.firstChild);
                        }
                        const data = JSON.parse(http.responseText);
                        data.sort((a, b) => {
                            if (a.eng < b.eng) return -1;
                            else return 1;
                        });
                        generateTable(data);

                    }
                };

                if(eng != null && rus!= null) {
                    if (rus.match(onlyRus)) {
                        if (rus.match(onlyRus)[0].length !== rus.length) {
                            alert('not correct rus word');
                        } else {
                            if (eng.match(onlyEng)) {
                                if (eng.match(onlyEng)[0].length !== eng.length) {
                                    alert('not correct eng word');
                                } else {
                                    http.send(`eng=${eng} &rus=${rus}`);
                                }
                            } else {
                                alert('not correct eng word');
                            }
                        }
                    } else {
                        alert('not correct rus word');
                    }
                }

            }

        }
    };
})();


'use strict';
const addword = (function() {
    return {
        init: function() {
            const button = document.getElementsByClassName('submit')[0];
            button.addEventListener('click', submitForm, false);
            function submitForm(event) {
                !event || event.preventDefault();
                const http = new XMLHttpRequest();
                http.open('POST', '/addword', true);
                http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

                const eng = document.getElementById('eng').value.trim();
                const rus = document.getElementById('rus').value.trim();

                const onlyRus = /[А-я]+/;
                const onlyEng = /[A-z]+/;

                if(eng != null && rus!= null) {
                    if (rus.match(onlyRus)) {
                        if (rus.match(onlyRus)[0].length !== rus.length) {
                            alert('not correct rus word');
                        } else {
                            if (eng.match(onlyEng)) {
                                if (eng.match(onlyEng)[0].length !== eng.length) {
                                    alert('not correct eng word');
                                } else {
                                    http.send(`eng=${eng} &rus=${rus}`);
                                }
                            } else {
                                alert('not correct eng word');
                            }
                        }
                    } else {
                        alert('not correct rus word');
                    }
                }

            }
        }
    };
})();