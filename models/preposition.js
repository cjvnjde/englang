'use strict';
const mongoose = require('../lib/mongoose'),
    Schema = mongoose.Schema;

const schema = new Schema({
    phrase: {
        type: String,
        required: true,
        trim: true
    },
    preposition: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    },
    prepositionIndex: {
        type: Number,
        required: true,
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

schema.statics.add = function(phrase, preposition, prepositionIndex)  {
    const Preposition = this;
    const prep = new Preposition({phrase: phrase, preposition: preposition, prepositionIndex: prepositionIndex});
    console.log(`new preposition ${phrase} => ${preposition} => ${prepositionIndex}`);
    prep.save()
        .catch(err => console.log(err));
};

schema.statics.random = function(callback){
    const Preposition = this;

    Preposition.count().exec(function(err, count) {
        const random = Math.floor(Math.random() * count);
        Preposition.findOne().skip(random).exec(function(err, result) {
            callback(result);
        });
    });
};

exports.Preposition = mongoose.model('Preposition', schema);