'use strict';
const mongoose = require('../lib/mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose.connection);

const schema = new Schema({
    eng: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    },
    rus: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});
schema.plugin(autoIncrement.plugin, 'Word');

schema.statics.add = function(eng, rus)  {
    const Word = this;
    if(eng.trim() !== '' && rus.trim() !== '') {
        const word = new Word({eng: eng, rus: rus});
        console.log(`new word ${eng} = ${rus}`);
        word.save()
            .catch(err => console.log(err));
    }
};

schema.statics.random = function(callback){
    const Word = this;
    Word.nextCount((err, count)=> {
        Word.find({_id: { $in: [
            Math.round(Math.random() * count),
            Math.round(Math.random() * count),
            Math.round(Math.random() * count)]
        } },(err, result) => {
            callback(result);
        });
    });
};

exports.Word = mongoose.model('Word', schema);