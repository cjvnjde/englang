'use strict';
const mongoose = require('../lib/mongoose'),
    Schema = mongoose.Schema;

const schema = new Schema({
    text: {
        type: String,
        required: true,
    },
    caption: {
        type: String,
        required: true,
    },
    author: {
        type: String,
        required: true,
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

schema.statics.add = function(caption, text, author)  {
    const Article = this;
    const article = new Article({text: text, author: author, caption: caption});
    article.save()
        .catch(err => console.log(err));
};

exports.Article = mongoose.model('Article', schema);