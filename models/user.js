'use strict';
const crypto = require('crypto');
const util = require('util');

const mongoose = require('../lib/mongoose'),
    Schema = mongoose.Schema;

const schema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true
    },
    accessRights: {
        type: Number,
        required: true,
        default: 0,
    },
    email: {
        type: String,
        required: true,
        trim: true
    },
    words: [
        {
            rus: String,
            eng: String
        }
    ],
    hashedPassword: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

schema.methods.encryptPassword = function(password) {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

schema.virtual('password')
    .set(function(password){
        this._plainPassword = password;
        this.salt = Math.random() + '';
        //console.log(this.encryptPassword);
        this.hashedPassword = this.encryptPassword(password);
    })
    .get(function() {this._plainPassword;});

schema.methods.checkPassword = function(password) {
    return this.encryptPassword(password) === this.hashedPassword;
};

schema.statics.authorize = function(username, password, callback)  {
    const User = this;

    User.findOne({username: username})
        .then(user => {
            if(user){
                if(user.checkPassword(password)) {
                    console.log('you were authorized');
                    callback(null, user);
                }else{
                    console.log('wrong password');
                    callback(new AuthError('Wrong password'));
                }
            }else{
                callback(new AuthError('User not exist'));
            }
        });
};
schema.statics.signup = function(username, password, email, callback)  {
    const User = this;

    User.findOne({username: username})
        .then(user => {
            if(user){
                callback(new AuthError('User already exist'));
            }else{
                const user = new User({username: username, password: password, email: email});
                console.log(`new user ${username}`);
                user.save()
                    .then((user) => _callback(user, callback))
                    .catch(err => console.log(err));
            }
        });
};


function _callback(user, callback){
    return new Promise(function(resolve, reject) {
        if(user) {
            callback(null, user);
            resolve();
        } else {
            reject();
        }
    });
}

exports.User = mongoose.model('User', schema);

function AuthError(message) {
    Error.apply(this, arguments);
    Error.captureStackTrace(this, AuthError);

    this.message = message;
}

util.inherits(AuthError, Error);

AuthError.prototype.name = 'AuthError';

exports.AuthError = AuthError;
