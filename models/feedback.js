'use strict';
const mongoose = require('../lib/mongoose'),
    Schema = mongoose.Schema;

const schema = new Schema({
    username: {
        type: String,
        required: true,
        trim: true
    },
    text: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

schema.statics.add = function(username, text)  {
    const Feedback = this;
    const feedback = new Feedback({username: username, text: text});
    feedback.save()
        .catch(err => console.log(err));
};


exports.Feedback = mongoose.model('Feedback', schema);