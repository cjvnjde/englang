'use strict';
const mongoose = require('../lib/mongoose'),
    Schema = mongoose.Schema,
    autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose.connection);

const schema = new Schema({
    infinitive: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    },
    pastSimple: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    },
    pastParticiple: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    },
    rus: {
        type: String,
        required: true,
        lowercase: true,
        trim: true
    }
});
schema.plugin(autoIncrement.plugin, 'IVerbs');

schema.statics.add = function(infinitive, pastSimple, pastParticiple, rus)  {
    const IVerbs = this;
    const iVerbs = new IVerbs({
        infinitive: infinitive,
        pastSimple: pastSimple,
        pastParticiple: pastParticiple,
        rus: rus
    });
    iVerbs.save()
        .catch(err => console.log(err));
};

schema.statics.random = function(callback){
    const IVerbs = this;
    IVerbs.nextCount((err, count)=> {
        IVerbs.find({_id: { $in: [
            Math.round(Math.random() * count),
            Math.round(Math.random() * count),
            Math.round(Math.random() * count)]
        } },(err, result) => {
            callback(result);
        });
    });
};

exports.IVerbs = mongoose.model('IVerbs', schema);