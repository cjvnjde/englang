'use strict';
const nodemailer = require('nodemailer');
const config = require('config');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: config.get('mail.sender.user'),
        pass: config.get('mail.sender.pass')
    }
});

/*const mailOptions = {
    to: 'papera351@gmail.com',
    subject: 'Sending Email using Node.js',
    text: 'That was easy!'
};*/

exports.mail = function(mailOptions) {
    mailOptions.from = config.get('mail.sender.user');
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
        } else {
            console.log('Email sent: ' + info.response);
        }
    });
};

