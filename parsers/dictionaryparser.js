'use strict';
const Word = require('../models/word').Word;

const onlyEng = /([A-z;, ()	-]+){1,10}/g;
const onlyRus = /([А-я;, ()	-]+){1,10}/g;

let eng = [];
let rus = [];

exports.parser = function() {
    const path = 'parsers/data/engwords.txt';

    const readline = require('readline');
    const fs = require('fs');




    Word.findOne().sort({id: -1}).exec((err, data) => {
        let id;
        if(!data){
            id = 0;
        }else{
            id=data.id;
        }
        const rl = readline.createInterface({
            input: fs.createReadStream(path)
        });
        rl.on('line', function (line) {
            //console.log(line);
            if(line) {
                let engWord = line.match(onlyEng)[0];
                let rusWord = line.match(onlyRus)[0];
                if (!rusWord.trim()) {
                    rusWord = line.match(onlyRus)[1];
                }
                if (rusWord && engWord)
                    Word.add(engWord, rusWord);
            }
        });
    });
};