"use strict";
const IVerbs = require('../models/irregularverbs').IVerbs;

const onlyEng = /([A-z;/]+)/g;
const onlyRus = /([А-я;,ё --()]+)/g;

exports.parser = function() {
    const path = 'parsers/data/irregularverbs.txt';

    const readline = require('readline');
    const fs = require('fs');

    const rl = readline.createInterface({
        input: fs.createReadStream(path)
    });

    rl.on('line', function (line) {
        if(line != '') {
            let infinitive = line.match(onlyEng)[0];
            let pastSimple = line.match(onlyEng)[1];
            let pastParticiple = line.match(onlyEng)[2];
            let rus = line.match(onlyRus);
            rus = rus[rus.length-1];
            IVerbs.add(infinitive, pastSimple, pastParticiple, rus);
        }
    });
};
