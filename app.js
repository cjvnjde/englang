'use strict';
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const stylus = require('stylus');
const sessionStore = require('./lib/sessionStore');
const config = require('config');
const session = require('express-session');
const HttpError = require('./error').HttpError;

//establish connection to mongoos
require('./lib/mongoose');
require('./error');

//require('./parsers/dictionaryparser').parser();
//require('./parsers/irregularverbsparser').parser();

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(require('./middleware/sendHttpError'));
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(stylus.middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));


app.use(session({
    secret: config.get('session.secret'),
    key: config.get('session.key'),
    cookie: config.get('session.cookie'),
    saveUninitialized: config.get('session.saveUninitialized'),
    resave: config.get('session.resave'),
    store: sessionStore
}));

app.use(require('./middleware/loadUser'));

//require all pages
require('./routes')(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});


app.use(function(err, req, res, next) {
    if(typeof err === 'number'){
        err = new HttpError(err);
    }

    if(err instanceof HttpError){
        res.sendHttpError(err);
    }else{
        if(app.get('env') === 'development'){
            res.locals.error = err;
        }else{
            err = new HttpError(500);
            res.sendHttpError(err);
        }
    }
});

module.exports = app;
