module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                stripBanner: true,
                banner: '/*name - <%= pkg.name %>; version - <%= pkg.version %>; date - <%= grunt.template.today("yyyy-mm-dd")%>*/\n'
            },
            dist: {
                src: ['public/javascripts/js/app.js',
                    'public/javascripts/js/games/*.js',
                    'public/javascripts/js/adminpanel/*.js',
                    'public/javascripts/js/partials/*.js',
                    'public/javascripts/js/auth/*.js',
                    'public/javascripts/js/pages/*.js',
                    'public/javascripts/js/add/*.js'],
                dest: 'public/javascripts/build/build.js'
            }
        },

        uglify: {
            compress: {
                drop_console: true,
            },
            build: {
                src: 'public/javascripts/build/build.js',
                dest: 'public/javascripts/build/build.min.js'
            }
        },

        watch: {
            scripts: {
                files: 'public/javascripts/js/**/*.js',
                tasks: ['concat', 'uglify']
            }

        }
    });


    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['concat', 'uglify', 'watch']);

};